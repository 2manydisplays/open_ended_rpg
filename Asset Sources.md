# Sources
## Mechanics
Point and Click Mechanics (buggy)

https://www.youtube.com/watch?v=MAbei7eMlXg

Point and Click Mechanics (working)

https://www.youtube.com/watch?v=CHV1ymlw-P8&t=2s

Dynamic NavMesh Components

https://github.com/Unity-Technologies/NavMeshComponents

Spawn random Objects on NavMesh

https://forum.unity.com/threads/generating-a-random-position-on-navmesh.873364/

Get closest Enemy

https://forum.unity.com/threads/clean-est-way-to-find-nearest-object-of-many-c.44315/



Animated Character

https://www.youtube.com/watch?v=blPglabGueM

Audio Across Scenes

https://www.youtube.com/watch?v=Xtfe5S9n4SI

Check if player is close

https://answers.unity.com/questions/443481/check-if-player-is-close.html

Make UI Elements Block Raycast

https://forum.unity.com/threads/canvas-group-not-blocking-raycasts-even-when-option-is-enabled.557584/

Inventory System E1 - E10

https://www.youtube.com/watch?v=S2mK6KFdv0I

Third Person Movement

https://www.youtube.com/watch?v=4HpC--2iowE

Camera Transition

https://www.youtube.com/watch?v=Ri8PEbD4w8A





## 3D Assets

2 Bed Apartment by grigor barseghyan
https://poly.google.com/view/bHGG6RV9oho

Jason's Isometric Room by Jason Toff
https://poly.google.com/view/8NsrLwbXhfl

Kitchen v3 by Sirkitree
https://poly.google.com/view/38PMRiku8qj

Plants - Assorted shelf plants by J
https://poly.google.com/view/5COCzyz489J

Blobby Volley 3D by sam-moshenko
https://github.com/sam-moshenko/Blobby-Volley-3D

Kira | Lowpoly Character by Akishaqs
https://assetstore.unity.com/packages/3d/characters/humanoids/humans/kira-lowpoly-character-100303

Liam | Lowpoly Character by Akishaqs
https://assetstore.unity.com/packages/3d/characters/humanoids/humans/liam-lowpoly-character-100007

Donut Box
https://blendswap.com/blend/15834

Food

https://assetstore.unity.com/packages/3d/props/food/free-casual-food-pack-mobile-vr-85884



## UI, Animations, Effects & Shaders

Typo

https://fonts.google.com/specimen/Contrail+One

Icons

https://github.com/feathericons/feather

Wobble Effect

https://assetstore.unity.com/packages/vfx/shaders/wobble-effects-shaders-105863

Shake Effect

https://gist.github.com/ftvs/5822103

See Through Walls Shader

https://www.youtube.com/watch?v=S5gdvibmsV0

Shower Particle Effect

https://assetstore.unity.com/packages/essentials/tutorial-projects/unity-particle-pack-127325

"Opening" Animation

https://www.mixamo.com/#/?genres=&page=1&query=opening

"Stand to Sit" Animation

https://www.mixamo.com/#/?genres=&page=1&query=stand+to+sit



## Sounds & Music

Music

https://www.chilloutmedia.com/lakey-inspired

Music Credit: LAKEY INSPIRED
Track Name: "EDIT TRACK NAME"
Music By: LAKEY INSPIRED 

Official SoundCloud https://soundcloud.com/lakeyinspired
Official YouTube Channel: https://www.youtube.com/channel/UCOmy8wuTpC95lefU5d1dt2Q
 License for commercial use: Creative Commons Attribution 3.0 Unported "Share Alike" (CC BY-SA 3.0) 
https://creativecommons.org/licenses/by-sa/3.0/legalcode
Music promoted by: Chill Out Records @ [https://goo.gl/fh3rEJ](https://www.youtube.com/redirect?q=https%3A%2F%2Fgoo.gl%2Ffh3rEJ&redir_token=6PToChsO5Ratbi10ka7XTMRCC3Z8MTU3MjExODI2OUAxNTcyMDMxODY5&v=2HiP-Sdtbck&event=video_description)

[www.ChillOutMedia.com](http://www.ChillOutMedia.com) / www.LoFi-HipHop.com

Music Credit: LAKEY INSPIRED
Track Name: "Acade" (CC BY-SA 3.0) 

https://soundcloud.com/lakeyinspired/Arcade