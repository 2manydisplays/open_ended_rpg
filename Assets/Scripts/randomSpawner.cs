using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class randomSpawner : MonoBehaviour
{     /// <summary>
      /// this script spawns object on a navMesh on random positions.
      /// </summary>


    private List<GameObject> objects;
    public int TotalNumberOfObjectsToSpawn;
    //public GameObject prefab;


    // Start is called before the first frame update
    void Awake()
    {
        objects = new List<GameObject>();

        SpawnAllObjects();
    }

    private void SpawnAllObjects()
    {
        ObjectRandomizer objectRandomizer = GetComponent<ObjectRandomizer>();

        // spawn all objects
        for (int personIndex = 0; personIndex < TotalNumberOfObjectsToSpawn; personIndex += 1)
        {
            // get a game location on the board
            Vector3 randomBoardLocation = GetRandomGameBoardLocation();

            // spawn a random person prefab at that location
            GameObject spawnedObject = SpawnObjectAtLocation(randomBoardLocation, objectRandomizer);

            //Debug.Log("Spawned " + spawnedPerson.name + " at " + randomBoardLocation);

            // add the spawned person to our collection
            objects.Add(spawnedObject);
        }
    }

    /// <summary>
    /// Selects a random point on the game board (NavMesh).
    /// </summary>
    /// <returns>Vector3 of the random location.</returns>
    private Vector3 GetRandomGameBoardLocation()
    {
        NavMeshTriangulation navMeshData = NavMesh.CalculateTriangulation();

        int maxIndices = navMeshData.indices.Length - 3;

        // pick the first indice of a random triangle in the nav mesh
        int firstVertexSelected = UnityEngine.Random.Range(0, maxIndices);
        int secondVertexSelected = UnityEngine.Random.Range(0, maxIndices);

        // spawn on verticies
        Vector3 point = navMeshData.vertices[navMeshData.indices[firstVertexSelected]];

        Vector3 firstVertexPosition = navMeshData.vertices[navMeshData.indices[firstVertexSelected]];
        Vector3 secondVertexPosition = navMeshData.vertices[navMeshData.indices[secondVertexSelected]];

        // eliminate points that share a similar X or Z position to stop spawining in square grid line formations
        if ((int)firstVertexPosition.x == (int)secondVertexPosition.x || (int)firstVertexPosition.z == (int)secondVertexPosition.z)
        {
            point = GetRandomGameBoardLocation(); // re-roll a position - I'm not happy with this recursion it could be better
        }
        else
        {
            // select a random point on it
            point = Vector3.Lerp(firstVertexPosition, secondVertexPosition, UnityEngine.Random.Range(0.05f, 0.95f));
        }

        return point;
    }

    /// <summary>
    /// Spawns a random object at the given spawn point.
    /// </summary>
    /// <param name="spawnPosition">The position to spawn the person at.</param>
    /// <param name="objectRandomizer">ObjectRandomizer that spawns objects.</param>
    /// <returns>The newly spawned person.</returns>
    private GameObject SpawnObjectAtLocation(Vector3 spawnPosition, ObjectRandomizer objectRandomizer)
    {
        // get a random person prefab to spawn
        GameObject randomPrefab = (GameObject)objectRandomizer.RandomObject();

        // spawn the person at the current spawn point
        GameObject randomObject = Instantiate(randomPrefab, spawnPosition, Quaternion.identity);     
       
        return randomObject;
    }
}
