using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.ThirdPerson;

public class PartnerController : MonoBehaviour
{
    //navigation variables
    public NavMeshAgent agent;    
    public bool inDialogue = false;
    int state = 0; // 0 = idle, 1 = wandering, 2 = talking, 3 = sitting, 4 = showering

    public GameObject[] targets;
    public Interactable focus;
    public ThirdPersonCharacter character;
    int currentTarget;

    private float nextActionTime = 0.0f;
    public float period = 1f;

    // Start is called before the first frame update
    void Start()
    {
        //rotation is controlled by animator
        agent.updateRotation = false;
        
        targets = GameObject.FindGameObjectsWithTag("Interactable");      

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene Appartment, LoadSceneMode mode)
    {
        targets = GameObject.FindGameObjectsWithTag("Interactable");
        Debug.Log("Scene is loaded");
    }

    void Update()
    {
        switch (state)
        {
            case 0://wandering
                setTarget();
                switchTarget();
                break;
            case 1: //stop walking and play talk animation

                break;
            case 2:
                //play sitting
                break;
        }
    }
    void switchTarget()
    {
        if (Time.time > nextActionTime)
        {
            nextActionTime += period;
            currentTarget += 1;
            if(currentTarget > targets.Length - 1)
            {
                currentTarget = 0;
            }
        }
    }
        void setTarget()
    {
        if (inDialogue == false)
        {
            agent.SetDestination(targets[currentTarget].transform.position);
            SetFocus(targets[currentTarget].GetComponent<Interactable>());
            //FaceTarget();
        }

        //agent.SetDestination(targets[currentTarget].transform.position);
        //SetFocus(targets[currentTarget].GetComponent<Interactable>());

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            character.Move(Vector3.zero, false, false);
        }        
    }
    void SetFocus(Interactable newFocus)
    {
        //check if we had another focus before and deactivate previous one
        if (newFocus != focus)
        {
            if (focus != null)
                focus.OnDeFocused();

            focus = newFocus;            
        }
        focus = newFocus;
        newFocus.OnFocused(transform);       
    }

    void RemoveFocus()
    {
        if (focus != null)
            focus.OnDeFocused();

        focus = null;      
    }
}
