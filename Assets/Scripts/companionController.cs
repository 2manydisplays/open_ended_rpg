using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class companionController : MonoBehaviour
{
    //navigation variables
    public NavMeshAgent agent;
    public ThirdPersonCharacter character;
    int currentTarget;
    public int state = 0; // 0 = stop, 1 = follow , 2 = hunt, 3 = wandering

    public GameObject[] targets;
    public Interactable focus;


    private GameObject player;
    private GameObject[] enemies;        


    // Start is called before the first frame update
    void Start()
    {
        targets = GameObject.FindGameObjectsWithTag("Interactable");
        player = GameObject.FindGameObjectWithTag("PlayerController");
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case 0://wandering
                stop();                
                break;
            case 1: //follow player
                setPlayerAsTarget();
                break;
            case 2: //hunt
                hunt();
                break;
            case 3: //move to targets                
                setTarget();
                break;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            switchBehaviour();
        }
    }

    void switchBehaviour()
    {
        state += 1;
        if (state > 3)
        {
            state = 0;
        }
    }

    void switchTarget()
    {
        currentTarget += 1;
        if (currentTarget > targets.Length - 1)
        {
            currentTarget = 0;
        }
    }

    void setTarget()
    {

        agent.SetDestination(targets[currentTarget].transform.position);
        SetFocus(targets[currentTarget].GetComponent<Interactable>());

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            character.Move(Vector3.zero, false, false);
            switchTarget();
        }
    }
    void SetFocus(Interactable newFocus)
    {
        //check if we had another focus before and deactivate previous one
        if (newFocus != focus)
        {
            if (focus != null)
                focus.OnDeFocused();

            focus = newFocus;
        }
        focus = newFocus;
        newFocus.OnFocused(transform);
    }

    void setPlayerAsTarget()
    {
        agent.SetDestination(player.transform.position);

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            character.Move(Vector3.zero, false, false);
            switchTarget();
        }
    }
    void hunt()
    {
        agent.SetDestination(GetClosestEnemy(enemies).transform.position);
        SetFocus(GetClosestEnemy(enemies).GetComponent<Interactable>());

        if (agent.remainingDistance > agent.stoppingDistance)
        {
            character.Move(agent.desiredVelocity, false, false);
        }
        else
        {
            character.Move(Vector3.zero, false, false);            
        }
    }

    private void stop()
    {
        agent.SetDestination(gameObject.transform.position);
        character.Move(Vector3.zero, false, false);
    }       

    GameObject GetClosestEnemy(GameObject[] enemies)
    {
        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject potentialTarget in enemies)
        {
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr && dSqrToTarget > 0)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
}
