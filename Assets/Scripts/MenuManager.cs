using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class MenuManager : MonoBehaviour
{
    //UI components
    //start menu
    public GameObject creditPanel;
    //character selection
    private string playerNameInput;
    private string partnerNameInput;
    GameObject player;
    GameObject partner;
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        partner= GameObject.FindGameObjectWithTag("NPC");
    }

    private void Start()
    {
        Debug.Log(PlayerNames.PlayerName);
        Debug.Log(PlayerNames.PartnerName);
    }

    //start menu
    public void showCreditPanel()
    {
        creditPanel.SetActive(true);
    }
    public void hideCreditPanel()
    {
        creditPanel.SetActive(false);
    }

    public void loadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //character menu
    public void loadPreviousScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void loadStartScene()
    {
        SceneManager.LoadScene("StartMenu", LoadSceneMode.Single);
    }

    public void loadPauseScene()
    {
        SceneManager.LoadScene("PauseMenu", LoadSceneMode.Additive);
    }

    public void quitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    //set player and partner names
    public void readPlayerName(string s)
    {
        playerNameInput = s;
        PlayerNames.PlayerName = playerNameInput;
    }
    public void readPartnerName(string s)
    {
        partnerNameInput = s;
        PlayerNames.PartnerName = partnerNameInput;
    }
    //deactivate player
    public void playerActive(bool b)
    {
        player.transform.GetChild(0).gameObject.SetActive(b);
    }
        //destroy characters
    public void destroyCharacters()
    {        
            Destroy(player);        
            Destroy(partner);        
    }
}
