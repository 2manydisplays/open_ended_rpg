using UnityEngine;
using UnityEngine.AI;

public class Interactable : MonoBehaviour
{
    public float radius = 1f;
    public Transform interactionTransform;

    bool isFocus = false;
    public bool hasInteracted = false;

    public Transform player;
    public Animator anim;
    //animation bools
    bool isSitting = false;
    bool isOpening = false;
    bool isPunching = false;
    bool isHit = false;
    Vector3 targetAnimationPosition;

    bool thirdPersonCamera = true;

    //timer
    public bool isCounting = false;
    public float timeLimit = 5f;
    private float counter;

    public virtual void Interact()
    {
        //overwrite this one
        Debug.Log("Interacting with " + transform.name);
    }

    public void OnFocused(Transform playerTransform)
    {
        isFocus = true;
        player = playerTransform;
        hasInteracted = false;
    }
    public void OnDeFocused()
    {
        isFocus = false;
        player = null;
        hasInteracted = false;
        if (isSitting)
        {
            isSitting = false;
            anim.SetBool("Sit", false);
        }
        if (isOpening)
        {
            isOpening = false;
            anim.SetBool("Open", false);
        }
        if (isPunching)
        {
            isPunching = false;
            anim.SetBool("Punch", false);
        }
        if (!thirdPersonCamera)
        {
            playerController.instance.switchCameras();
        }        
    }
    private void Update()
    {
        if (isFocus && !hasInteracted)
        {
            float distance = Vector3.Distance(player.position, interactionTransform.position);
            if (distance <= radius)
            {
                Interact();               
                hasInteracted = true;                
            }
        }

        //if character is supposed to stand up after hit
        if (isHit && isCounting)
        {
            counter += Time.deltaTime;

            if (counter >= timeLimit)
            {
                anim = interactionTransform.GetComponent<Animator>();
                anim.SetBool("isHit", false);
                isCounting = false;
                isHit = false;
                counter = 0;
            }
        }

        //lerp player to the right seating position
        if (isSitting)
        {           
            player.transform.position = Vector3.Slerp(player.transform.position, targetAnimationPosition, Time.deltaTime * 5);
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, transform.rotation, Time.deltaTime * 5);
        }

        //lerp player to the right interaction position
        if (isOpening)
        {            
            player.transform.position = Vector3.Slerp(player.transform.position, targetAnimationPosition, Time.deltaTime * 5);
            player.transform.rotation = Quaternion.Slerp(player.transform.rotation, transform.rotation, Time.deltaTime * 5);
        }
    }

    private void OnDrawGizmosSelected()
    {
        if (interactionTransform == null)
        {
            interactionTransform = transform;
        }

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(interactionTransform.position, radius);
    }

    public virtual void triggerSittingAnimation()
    {
        isSitting = true;
        anim = player.GetComponent<Animator>();
        player.GetComponent<NavMeshAgent>().updateRotation = false;
        targetAnimationPosition = new Vector3(interactionTransform.position.x, player.position.y, interactionTransform.position.z);
        anim.SetTrigger("Sit");        
    }
    public virtual void triggerOpeningAnimation()
    {
        isOpening = true;
        anim = player.GetComponent<Animator>();
        player.GetComponent<NavMeshAgent>().updateRotation = false;
        targetAnimationPosition = new Vector3(interactionTransform.position.x, player.position.y, interactionTransform.position.z);
        anim.SetTrigger("Open");
    }
    public virtual void triggerPunchAnimation()
    {
        isPunching = true;
        anim = player.GetComponent<Animator>();
        player.GetComponent<NavMeshAgent>().updateRotation = false;
        anim.SetTrigger("Punch");
        //player.GetComponent<playerController>().FaceTarget();
    }
    public virtual void triggerEnemyHitAnimation()
    {
        isHit = true;
        anim = interactionTransform.GetComponent<Animator>();
        Debug.Log("animator name " + anim.gameObject.name);
        anim.SetTrigger("isHit");
    }

    public virtual void triggerLieAnimation()
    {
        Debug.Log("Character lies down");
    }

    public virtual void switchCameras()
    {
        thirdPersonCamera = false;
        player.GetComponent<playerController>().switchCameras();        
    }
       
}
