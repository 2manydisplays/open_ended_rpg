using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Interactable
{
    myEnemyController controller;

    public override void Interact()
    {
        base.Interact();
        base.triggerPunchAnimation();
        base.triggerEnemyHitAnimation();
        updateState();
        isCounting = true; //start timer to make character get up again
    }

    void updateState()
    {
        controller = interactionTransform.GetComponent<myEnemyController>();
        controller.state = 1; //Set state to 1 = stop
    }
   
}
