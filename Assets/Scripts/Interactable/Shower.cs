using UnityEngine;

public class Shower : Interactable
{
    public GameObject shower;
    public override void Interact()
    {
        base.Interact();
        base.triggerOpeningAnimation();
        toggleShower();
    }

    void toggleShower()
    {
        shower.SetActive(!shower.activeSelf);
    }
    //activate/deactivate selection icon on mouse over
    private void OnMouseOver()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }
    void OnMouseExit()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }
}
