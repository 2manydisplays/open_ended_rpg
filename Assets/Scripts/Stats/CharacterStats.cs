using UnityEngine;
using UnityEngine.UI;

public class CharacterStats : MonoBehaviour
{
    public bool level3;
    //Simple Singleton Pattern
    public static CharacterStats instance;
  
    public Slider energySlider;
    public int maxEnergy;
    public int minEnergy;
    public int currentEnergy { get; private set; }

    public Slider motivationSlider;
    public int maxMotivation;
    public int minMotivation;
    public int currentMotivation { get; private set; }

    public Stat damage;
    public Stat energy;
    public Stat armor;
    public Stat motivation;



    private void Awake()
    {
        instance = this;

        currentEnergy = maxEnergy;
        energySlider.maxValue = currentEnergy;
        energySlider.value = currentEnergy;
   
        currentMotivation = maxMotivation;
        motivationSlider.maxValue = maxMotivation;
        motivationSlider.value = currentMotivation;

        if (level3)
        {
            DamageEnergy(50);
            DamageMotivation(100);
        }
        
    }

    public void DamageEnergy(int damage)
    {
        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 0, int.MaxValue);

        currentEnergy -= damage;
        energySlider.value = currentEnergy;
        Debug.Log(transform.name + " takes " + damage + " energy damage.");
    
        if(currentEnergy <= 0)
        {
            currentEnergy = minEnergy;
            energySlider.value = minEnergy;
            pleaseSomeoneKillMe(); 
        }
    }
    public void DamageMotivation(int damage)
    {
        //damage -= armor.GetValue();
        //damage = Mathf.Clamp(damage, 0, int.MaxValue);

        currentMotivation -= damage;
        motivationSlider.value = currentMotivation;
        Debug.Log(transform.name + " takes " + damage + " motivation damage.");

        if (currentMotivation <= 0)
        {
            currentMotivation = minMotivation;
            motivationSlider.value = minMotivation;
            pleaseSomeoneKillMe();
        }
    }

    public void addEnergy(int energy)
    {
        currentEnergy += energy;
        energySlider.value = currentEnergy;
        Debug.Log(transform.name + " adds " + energy + " energy.");

        if (currentEnergy >= maxEnergy)
        {
            currentEnergy = maxEnergy;
            energySlider.value = maxEnergy;

            QuestManager.instance.completeQ1();
        }             
    }
    public void addMotivation(int motivation)
    {
        currentMotivation += motivation;
        motivationSlider.value = currentMotivation;
        Debug.Log(transform.name + " adds " + motivation + " motivation.");

        if (currentMotivation >= maxMotivation)
        {
            currentMotivation = maxMotivation;
            motivationSlider.value = maxMotivation;

            QuestManager.instance.completeQ2();
        }
    }

    public virtual void pleaseSomeoneKillMe()
    {
        //this method is ment to be overwritten
        Debug.Log(transform.name + " died.");
    }    
}
