using UnityEngine;
using UnityEngine.UI;

public class CustomizationManager : MonoBehaviour
{
    public GameObject[] bodyTypes;
    public Avatar[] avatars;

    public int currentIndex = 0;  

    public Button nextBodyType;
    public Button previosBodyType;

    public Animator playerAnimator;
    
    

    void Start()
    {
        bodyTypes[currentIndex].SetActive(true);
        nextBodyType.onClick.AddListener(() => { changeBodyType(true); });
        previosBodyType.onClick.AddListener(() => { changeBodyType(false); });
    }

    public void changeBodyType(bool increase)
    {
        bodyTypes[currentIndex].SetActive(false);
        currentIndex += increase ? 1 : -1;

        if (currentIndex > bodyTypes.Length - 1)
            currentIndex = 0;
        if (currentIndex < 0)
            currentIndex = bodyTypes.Length - 1;

        bodyTypes[currentIndex].SetActive(true);
        playerAnimator.avatar = avatars[currentIndex];
    }    
}
