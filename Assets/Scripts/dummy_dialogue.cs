using UnityEngine;

public class dummy_dialogue : MonoBehaviour
{
    DialogueManager dialogueManager;

    // Start is called before the first frame update
    void Start()
    {
        dialogueManager = GameObject.FindGameObjectWithTag("DialogueManager").gameObject.GetComponent<DialogueManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PlayerController"))
        {
            //activate dialog
            dialogueManager.activateDialogueBox();
        }
    }
}
