using Ink.Runtime;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class npcDialogue : Interactable
{
    public NPC npc;

    public TextAsset inkJSON;
    private Story story;

    public GameObject dialogueBox;

    public TextMeshProUGUI textPrefab;
    public Button buttonPrefab;
    public Canvas canvas;

    public override void Interact()
    {
        base.Interact();
        //activateDialogueBox();

        //get story file
        story = new Story(inkJSON.text);

        refreshUI();


        //Debug.Log(loadStoryChunk());
        Debug.Log("number of choices "+story.currentChoices);

    }
    void refreshUI()
    {
        eraseUI();

        TextMeshProUGUI storyText = Instantiate(textPrefab) as TextMeshProUGUI;
        storyText.text = loadStoryChunk();
        storyText.transform.SetParent(canvas.transform, false);

        //load story chunk
        Debug.Log(loadStoryChunk());

        foreach (Choice choice in story.currentChoices)
        {
            
            Button choiceButton = Instantiate(buttonPrefab) as Button;        
            choiceButton.transform.SetParent(canvas.transform, false);

            TextMeshProUGUI choiceText = choiceButton.GetComponentInChildren<TextMeshProUGUI>();
            choiceText.text = choice.text;

            choiceButton.onClick.AddListener(delegate   
            { 
                chooseStoryChoice(choice);
            });
        }
    }
    void chooseStoryChoice(Choice choice)
    {
        story.ChooseChoiceIndex(choice.index);
        refreshUI();
    }

    void eraseUI()
    {
        for (int i = 0; i< canvas.transform.childCount; i++)
        {
            Destroy(canvas.transform.GetChild(i).gameObject);
        }
    }

    string loadStoryChunk()
    {
        string text = "";

        //check if story can continue (has multiple lines)
        if (story.canContinue)
        {
            text = story.ContinueMaximally();
        }
        return text;
    }

    public void activateDialogueBox()
    {
        dialogueBox.SetActive(true);
        //deactivate player control
        playerController.instance.inDialogue = true;
        Debug.Log("player is in dialogue = " + playerController.instance.inDialogue);
    }

    public void deactivateDialogueBox()
    {
        dialogueBox.SetActive(false);
        //activate player control
        playerController.instance.inDialogue = false;
    }
}
