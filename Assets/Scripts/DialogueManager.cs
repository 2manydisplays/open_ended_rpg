using UnityEngine;

public class DialogueManager : MonoBehaviour
{    
    public GameObject dialogueBox;   

    public void activateDialogueBox()
    {
        dialogueBox.SetActive(true);
        //deactivate player control
        playerController.instance.inDialogue = true;       
    }

    public void deactivateDialogueBox()
    {
        dialogueBox.SetActive(false);
        //activate player control
        playerController.instance.inDialogue = false;
    }
}
