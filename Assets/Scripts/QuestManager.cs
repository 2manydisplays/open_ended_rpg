using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestManager : MonoBehaviour
{   //Simple Singleton Pattern
    public static QuestManager instance;

    public GameObject square;
    public GameObject square_check;

    public GameObject square2;
    public GameObject square_check2;

    bool q1_completed = false;
    bool q2_completed = false;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.Log("More than one instance of QuestManager");
        }
        instance = this;
    }

    // Update is called once per frame
    public void completeQ1()
    {
        if (!q1_completed)
        {
            square.SetActive(!square.activeSelf);
            square_check.SetActive(!square.activeSelf);
            q1_completed = true;
        }
        
    }

    public void completeQ2()
    {
        if (!q2_completed)
        {
            square2.SetActive(!square2.activeSelf);
            square_check2.SetActive(!square2.activeSelf);
            q2_completed = true;
        }
    }
}
