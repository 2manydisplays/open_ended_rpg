using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    Vector3 targetPosition;
    Vector3 lookAtTarget;
    Quaternion newPlayerRot;
    Rigidbody rb;
        
    public float rotSpeed = 1;
    public float speed = 1;

    bool isMoving = false;
    bool isWaiting = false;

    float counter = 0;
    bool isCounting = true;

    public NavMeshAgent agent;

    [Tooltip("If checked player needs a NavAgent and movement won't buggy")]
    public bool usingNavAgent = false; //if player should move buggy set t

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // isMoving = animator.GetBool("isMoving");

        if (Input.GetMouseButton(0) && !usingNavAgent) 
        {
            setTargetPosition();
        }

        if (Input.GetMouseButton(0) && usingNavAgent)
        {
            setAgentTarget();
        }

        if (isMoving == true)
        {
            updateMovement();
        }

        /*
        if (isWaiting == true)
        {
            rotateToCamera();
        }
        Timer();
        */
    }

    //this code is a bit buggy
    void setTargetPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        
            if (Physics.Raycast(ray, out hit, 1000))
            {
                targetPosition = hit.point;
                // transform.LookAt(targetPosition);
                lookAtTarget = new Vector3(targetPosition.x - transform.position.x, transform.position.y, targetPosition.z - transform.position.z);
                newPlayerRot = Quaternion.LookRotation(lookAtTarget);
            }     

        isMoving = true;
        isWaiting = false;        
    }

    void updateMovement()
    {
        transform.rotation = Quaternion.Slerp(transform.rotation, newPlayerRot, rotSpeed * Time.deltaTime);
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);

        if (transform.position == targetPosition)
        {
            isMoving = false;
            isCounting = true;           
        }
    }

    //this code is not buggy
    void setAgentTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            agent.SetDestination(hit.point);
        }
    }

        void Timer()
    {
        if (!isMoving && isCounting == true)
        {
            counter += Time.deltaTime;
            Debug.Log(counter);

            if (counter >= 1)
            {
                isCounting = false;
                counter = 0;

                isWaiting = true;
            }
        }

    }

    public void rotateToCamera()
    {
        lookAtTarget = new Vector3(Camera.main.transform.position.x - transform.position.x, transform.position.y, Camera.main.transform.position.z - transform.position.z);
        newPlayerRot = Quaternion.LookRotation(lookAtTarget);
        transform.rotation = Quaternion.Slerp(transform.rotation, newPlayerRot, rotSpeed * Time.deltaTime);
    }
}