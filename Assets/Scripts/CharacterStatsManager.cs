using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CharacterStatsManager : MonoBehaviour
{
    public Slider energy;
    public float maxEnergy = 10;    
    public float currentEnergy = 1;

    public Slider motivation;
    public float maxMotivation = 10;
    public float currentMotivation = 1;

    // Start is called before the first frame update
    void Start()
    {
        //set energy and motivation values
        energy.maxValue = maxEnergy;
        energy.value = currentEnergy;
        motivation.maxValue = maxMotivation;
        motivation.value = currentMotivation;

        // if energy is low: energy.fillRect.gameObject.SetActive(false);
    }

    void addEnergy (float amount)
    {
        currentEnergy += amount;        
        energy.value = currentEnergy;

        if (currentEnergy >= maxEnergy)
        {
            energy.value = maxEnergy;
        }
    }

    void addMotivation(float amount)
    {
        currentEnergy += amount;
        energy.value = currentMotivation;

        if (currentMotivation >= maxMotivation)
        {
            motivation.value = maxMotivation;           
        }
    }
}
