using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;

public class myEnemyController : MonoBehaviour
{
    //navigation variables
    public NavMeshAgent agent;
    public ThirdPersonCharacter character;

    public int state = 0;

    private GameObject[] enemies;
    private GameObject companion;

    public float avoidanceRadius = 1f;
    public float fleeRadius = 3f;

    // Start is called before the first frame update
    void Start()
    {       
        enemies = GameObject.FindGameObjectsWithTag("Enemy");
        companion = GameObject.FindGameObjectWithTag("Companion");
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case 0://flocking
                flock();                
                break;
            case 1: //stop
                stop();
                break;                
        }
    }

    void stop()
    {
        agent.SetDestination(gameObject.transform.position);
        character.Move(Vector3.zero, false, false);
    }

    void flock()
    {    
        float dist = Vector3.Distance(transform.position, GetClosestEnemy(enemies).transform.position);
        float distToCompanion = Vector3.Distance(transform.position, companion.transform.position);

        character.Move(agent.desiredVelocity, false, false);

        //move to each other
        if (dist > avoidanceRadius && distToCompanion > fleeRadius)
        {
            agent.destination = this.transform.position - GetClosestEnemy(enemies).transform.position.normalized;           
        }

        //avoid each other
        if (dist < avoidanceRadius )
        {
            agent.destination = this.transform.position + GetClosestEnemy(enemies).transform.position.normalized;
        }

        //flee
        if (distToCompanion < fleeRadius)
        {
            agent.destination = this.transform.position + companion.transform.position.normalized;
        }
        
    }

    //calculate closest enemy
    GameObject GetClosestEnemy(GameObject[] enemies)
    {
        GameObject bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (GameObject potentialTarget in enemies)
        {
            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr && dSqrToTarget > 0)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        return bestTarget;
    }
}
