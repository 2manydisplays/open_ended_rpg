using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiShake : MonoBehaviour
{
	// Transform of the rect to shake. Grabs the gameObject's transform
	// if null.
	public RectTransform rectTransform;

	// How long the object should shake for.
	public float shakeDuration = 0f;

	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;

	Vector3 originalPos;

	void Awake()
	{
		if (rectTransform == null)
		{
			rectTransform = GetComponent(typeof(RectTransform)) as RectTransform;
		}
	}

	void OnEnable()
	{
		originalPos = rectTransform.anchoredPosition;
	}

	void Update()
	{
		if (shakeDuration > 0)
		{
			rectTransform.anchoredPosition = originalPos + Random.insideUnitSphere * shakeAmount;

			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shakeDuration = 0f;
			rectTransform.anchoredPosition = originalPos;
		}
	}
}
