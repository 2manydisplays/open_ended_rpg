using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    GameObject player;
    GameObject npc;

    public GameObject npcStartposition;
    public GameObject playerStartposition;


    private void Awake()
    {
            player = GameObject.FindGameObjectWithTag("Player");
            npc = GameObject.FindGameObjectWithTag("NPC");        
    }

    // Start is called before the first frame update
    void Start()
    {
        if (player != null)
        {
            //activate all player controller components       
            player.transform.GetChild(0).gameObject.GetComponent<Animator>().enabled = true;
            player.transform.GetChild(0).gameObject.GetComponent<playerController>().enabled = true;
            player.transform.GetChild(0).gameObject.GetComponent<ThirdPersonCharacter>().enabled = true;
            player.transform.GetChild(0).gameObject.GetComponent<NavMeshAgent>().enabled = true;
            player.transform.GetChild(0).gameObject.GetComponent<SeeThroughSync>().enabled = true;

            //activate player ui   
            player.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
            Debug.Log("No Player found");

        //set npc startposition
        if (npcStartposition != null)
        {
            npc.transform.position = npcStartposition.transform.position;
        }
        else
            Debug.Log("No NPC Startposition is set");

        if (playerStartposition != null)
        {
            player.transform.position = playerStartposition.transform.position;
        }
        else
            Debug.Log("No Player Startposition is set");

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
