using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceAnimationController : MonoBehaviour
{
    int blendShapeCount;
    public SkinnedMeshRenderer eyes;
    Mesh skinnedMesh;
    float blendOne = 0f;
    float blendTwo = 0f;
    float blendSpeed = 10f;
    bool blendOneFinished = false;
    void Awake()
    {        
        skinnedMesh = eyes.sharedMesh;
    }
    // Start is called before the first frame update
    void Start()
    {
        blendShapeCount = skinnedMesh.blendShapeCount;
    }

    void Update()
    {
        
          eyes.SetBlendShapeWeight(0, blendOne);
          blendOne = Mathf.PingPong(blendSpeed, 100); ;
       
            if (blendOneFinished == true && blendTwo < 100f)
            {
                eyes.SetBlendShapeWeight(1, blendTwo);
                blendTwo += blendSpeed;
            }
    }

}
