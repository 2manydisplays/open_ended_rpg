using UnityEngine;

public class SeeThroughSync : MonoBehaviour
{
    public static int PosID = Shader.PropertyToID("_position");
    public static int sizeID = Shader.PropertyToID("_size");

    public Material Wallmaterial;    
    public LayerMask Mask;

    private void Update()
    {
        var dir = Camera.main.transform.position - transform.position;
        var ray = new Ray(transform.position, dir.normalized);
        RaycastHit hit;

        Debug.DrawLine(Camera.main.transform.position, transform.position);

        if (Physics.Raycast(ray,3000, Mask))
        {            
            Wallmaterial.SetFloat(sizeID, 3);
        }
        else
        {
            Wallmaterial.SetFloat(sizeID, 0);
        }           

        var view = Camera.main.WorldToViewportPoint(transform.position);
        Wallmaterial.SetVector(PosID, view);
    }


}
